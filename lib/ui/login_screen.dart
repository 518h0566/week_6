import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static const route = '/login';
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }

}



class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('stopwatch v.0.0.6'),),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          fieldEmailAddress(),
          Container(margin: EdgeInsets.only(top: 20.0),),
          fieldPassword(),
          Container(margin: EdgeInsets.only(top: 40.0),),
          loginButton()
        ],
      ),
    );
  }



  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return 'Email must be at least character @.';
        }
        return null;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: (value) {
        if (value!.length < 8) {
          return 'Password must be at least 8 characters.';
        }
        if (!value.contains(RegExp(r"[A-Z]"))) {
          return 'Password must be at least 1 upper-case letter characters.';
        }
        if (!value.contains(RegExp(r"[a-z]"))) {
          return 'Password must be at least 1 normal characters.';
        }
        if (!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
          return 'Password must be at least 1 special characters.';
        }

        return null;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: validate,
        child: Text('Login'));
  }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context).pushReplacementNamed('/stopwatch', arguments: email);
    }
  }
}